
/**
 * Solves the n-ary birthday problem both theoretically and practically
 * @param {*} population the number of choices to make (default 23)
 * @param {*} base the number of options the population has to pick from (default 365)
 * @param {*} iterations the number of random populations generated to calculate the value practically (default 100000)
 */
function birthday (population = 23, base = 365, iterations = 100000) {
    if (population < 2 || population > base) {
        throw new Error(`
====================================================================================

Length must be in range [2-${base}].  Instead got ${population}.
A population of 1 or less will have a 0% chance of duplicates.
A population size greater than the number of options (${base}) will have a 100% chance of duplicates.

====================================================================================
        `)
    }

    console.log(`Out of a population of ${population}, each independently choosing from ${base} options...`)

    function getExpectedProbability () {
        let probabilityOfFailure = 1
        for (let i=1; i<population; i++) {
            probabilityOfFailure *= (base - i) / base
        }

        return 1 - probabilityOfFailure
    }

    function randomInt () {
        return Math.floor(Math.random() * base)
    }

    function makeNumbers () {
        const numbers = []
        for (let i=0; i<population; i++) {
            numbers.push(randomInt())
        }
        return numbers
    }

    function hasDuplicate (numbers) {
        for (let i=0; i<numbers.length - 1; i++) {
            for (let j=i+1; j<numbers.length; j++) {
                if (numbers[i] === numbers[j]) {
                    return true
                }
            }
        }

        return false
    }

    const expectedPercent = getExpectedProbability() * 100
    console.log(`The expected probability of duplicates is ${expectedPercent.toFixed(2)}%`)

    let duplicateCount = 0

    for (let i=0; i<iterations; i++) {
        const nums = makeNumbers()
        if (hasDuplicate(nums)) {
            duplicateCount++
        }
    }

    const percent = duplicateCount / iterations * 100
    console.log(`${percent.toFixed(2)}% of the population had duplicates.`)
    console.log(`(${duplicateCount} / ${iterations})`)
}

const args = process.argv.slice(2).map(a => parseInt(a, 10))
birthday(...args)
